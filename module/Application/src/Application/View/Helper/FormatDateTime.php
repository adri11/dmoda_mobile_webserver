<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class FormatDateTime extends AbstractHelper {

    public function __invoke($dateString,$type='date') {
        if ($dateString==null) {
            return 'Assente';
        }
        $date=new \DateTime($dateString);
        //check if it's a not setted date (like "0000-00-00")
        if ($date<(new \DateTime('1000-01-01'))) {
            return 'Assente';
        }
        if ($type==='datetime') {
            return $date->format('G:i d/m/Y');
        }
        else if ($type==='time') {
            return $date->format('G:i');
        }
        else if ($type==='date'){
            return $date->format('d/m/Y');
        }
    }
}