<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class GetRole extends AbstractHelper {

    protected $sm;

    public function __construct($sm) {
        $this->sm = $sm;
    }

    public function __invoke() {
        $authService=$this->sm->getServiceLocator()->get('AuthService');
        if ($authService->hasIdentity()) {
            $role=$authService->getStorage()->getRole();
        }
        else {
            $role='public';
        }
        return $role;
    }
}