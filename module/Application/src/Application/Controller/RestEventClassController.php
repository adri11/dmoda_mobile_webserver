<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractRestfulController;
//use Application\Service\Auth;
use Application\Model\DbLegacy;
use Application\Form\UserForm;
use Application\Form\UserFilter;
use Application\Form\EventClassForm;
use Application\Form\EventClassFilter;
use Application\Form\MessageClassForm;
use Application\Form\MessageClassFilter;
use Application\Form\UserMessageClassForm;
use Application\Model\User;
use Application\Model\EventClass;
use Application\Model\MessageClass;
use Application\Model\UserHasMessageClass;
use Application\Model\EventClassHasStore;
use Application\Model\MessageSpool;
use Application\Model\MessageArchive;
use Application\Model\EventArchive;
use Zend\View\Model\JsonModel;

use DateTime;

/**
 * Class RestEventClassController
 * @package Application\Controller
 * Url: /rest/event-class
 */
class RestEventClassController extends RestCommonController {

    public function getTable($tableName)
    {
        //inizialization, just put for don't get IDE warning
        $riferimento=null;

        //$riferimento points to $this->$tableName.'Table'
        eval ("\$riferimento = &\$this->".$tableName."Table;");
        if (!$riferimento) {
            $sm = $this->getServiceLocator();
            $riferimento = $sm->get('Application\Model\DbTable\\'.$tableName.'Table');
        }
        return $riferimento;
    }

    public function get($id)
    {
        return $this->returnError('Not allowed');
    }

    public function getList()
    {
        $restToken=$this->params()->fromQuery('rest-token');
        $userDetail=$this->getTable('user')->getUserDetailByRestToken($restToken);
        if ($userDetail==null) {
            return $this->returnError('Rest token not valid');
        }

        $eventClassList=$this->getTable('EventClass')->fetchAllExisting();
        $dbLegacy=new DbLegacy();
        $dbLegacy->connect();
        $storesList=$dbLegacy->getStoresList();
        $dbLegacy->close();
        $eventClassListArray=array();
        foreach ($eventClassList as $eventClassListEl) {
            $storeDetailList=array();
            if ($eventClassListEl->event_class_structured_all_stores) {
                $storeDetailList[]['legacy_db_store_description']="Tutti";
            }
            else {
                $eventClassStoresIds=$this->getTable('EventClassHasStore')->getListByEventClass($eventClassListEl->event_class_id);
                foreach ($eventClassStoresIds as $eventClassStoresIdsEl) {
                    foreach ($storesList as $storesListEl) {
                        if ($storesListEl['legacy_db_store_id']===$eventClassStoresIdsEl->legacy_db_store_id) {
                            $storeDetailList[]=$storesListEl;
                            break;
                        }
                    }
                }
            }
            $eventClassListEl->storeDetailList=$storeDetailList;
            $eventClassListArray[]=$eventClassListEl;
        }
        $modello=new JsonModel($eventClassListArray);
        return $this->getResponseWithHeader()->setContent($modello->serialize());
    }

    public function create($data)
    {
        return $this->returnError('Not allowed');
    }

    public function update($id, $data)
    {
        return $this->returnError('Not allowed');
    }

     public function delete($id)
    {
        return $this->returnError('Not allowed');
    }
}

