<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractRestfulController;
//use Application\Service\Auth;
use Application\Model\DbLegacy;
use Application\Form\UserForm;
use Application\Form\UserFilter;
use Application\Form\EventClassForm;
use Application\Form\EventClassFilter;
use Application\Form\MessageClassForm;
use Application\Form\MessageClassFilter;
use Application\Form\UserMessageClassForm;
use Application\Model\User;
use Application\Model\EventClass;
use Application\Model\MessageClass;
use Application\Model\UserHasMessageClass;
use Application\Model\EventClassHasStore;
use Application\Model\MessageSpool;
use Application\Model\MessageArchive;
use Application\Model\EventArchive;
use Zend\View\Model\JsonModel;
use DateTime;

/**
 * Handling of the messages subscriptions
 * To be used it requires the communication of the Rest Token
 * Url: /rest/messages-subscription
 *
 * @package Application\Controller
 */
class RestMessagesSubscriptionController extends RestCommonController {
    public function getTable($tableName)
    {
        //inizialization, just put for don't get IDE warning
        $riferimento=null;

        //$riferimento points to $this->$tableName.'Table'
        eval ("\$riferimento = &\$this->".$tableName."Table;");
        if (!$riferimento) {
            $sm = $this->getServiceLocator();
            $riferimento = $sm->get('Application\Model\DbTable\\'.$tableName.'Table');
        }
        return $riferimento;
    }

    public function get($id)
    {
        $restToken=$id;
        $userDetail=$this->getTable('user')->getUserDetailByRestToken($restToken);
        if ($userDetail==null) {
            return $this->returnError('Rest token not valid');
        }

        $eventClassId=$this->params()->fromQuery('event-class-id');
        if ($eventClassId==null) {
            return $this->returnError ('Event class non specified');
        }


        $userId=$userDetail->user_id;
        $messageClassListWithInterval=array();
        $messageClassList=$this->getTable('MessageClass')->fetchAll($eventClassId);
        for ($i=0;$i<$messageClassList->count();$i++) {
            $messageClassDetail=$messageClassList->current();
            $messageClassId=$messageClassDetail->message_class_id;
            $resultSet=$this->getTable('UserHasMessageClass')->fetchAll($userId,$messageClassId);
            if ($resultSet->count()!=0) {
                $messageClassDetail->user_has_message_class_interval=$resultSet->current()->user_has_message_class_interval;
            }
            else {
                //Non iscritto all'evento
                $messageClassDetail->user_has_message_class_interval=999;
            }
            $messageClassList->next();
            $messageClassListWithInterval[]=$messageClassDetail;
        }

        $modello=new JsonModel($messageClassListWithInterval);
        return $this->getResponseWithHeader()->setContent($modello->serialize());

    }

    public function getList()
    {
        return $this->returnError('Not allowed');
    }


    public function create($data)
    {
        return $this->returnError('Not allowed');
    }

    /**
     * User subscribes to a message class or cancel a subscription (interval=999)
     * @param mixed $id Message class id
     * @param mixed $data Message notification interval and rest token
     * @return mixed
     */
    //testato
    public function update($id, $data)
    {
        $messageClassId=$id;
        $interval=$data['interval'];

        $restToken=$data['rest-token'];
        $userDetail=$this->getTable('user')->getUserDetailByRestToken($restToken);
        if ($userDetail==null) {
            return $this->returnError('Rest token not valid');
        }
        $userId=$userDetail->user_id;

        $this->getTable('UserHasMessageClass')->deleteElement($userId,$messageClassId);
        $dateTime=new DateTime();
        $dateTime->setTimezone(new \DateTimeZone('Europe/Rome'));

        if ($interval!=999) {
            $dati['user_id']=$userId;
            $dati['message_class_id']=$messageClassId;
            $dati['user_has_message_class_interval']=$interval;
            $dati['user_has_message_class_last_send']= $dateTime->format('Y-m-d G:i:s');
            $this->getTable('UserHasMessageClass')->addElementArray($dati);

        }

        $jsonModel=new JsonModel(array(
            'status'=>'ok'
        ));
        return $this->returnJsonWithHeader($jsonModel);
    }

    /**
     * User deletes a subscription to a message class
     * @param mixed $id Message class id
     * @return mixed
     */

    public function delete($id)
    {
        return $this->returnError('Not allowed');
    }

    // configure response
    public function getResponseWithHeader()
    {
        $response = $this->getResponse();
        $response->getHeaders()
        //make can accessed by *
            ->addHeaderLine('Access-Control-Allow-Origin','*')
        //set allow methods
            ->addHeaderLine('Access-Control-Allow-Methods','POST PUT DELETE GET');

        return $response;
    }


}

