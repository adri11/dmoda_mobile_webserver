<?php
//TODO Nella parte del demone va fatta verifica che il messaggio generato sia inferiore a 1000 caratteri, altrimenti inviare messaggio di errore all'utente o altro

namespace Application\Controller;

use Application\Service\Auth;
use Application\Service\AuthStorage;
use Zend\Authentication\AuthenticationService;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use Zend\Session\Container;
use Zend\Session\Storage\ArrayStorage;
use Zend\Session\Storage\SessionStorage;
use Zend\View\Model\ViewModel;
//use Application\Service\Auth;
use Application\Model\DbLegacy;
use Application\Form\UserFormSuper;
use Application\Form\UserFormRegular;
use Application\Form\UserFilter;
use Application\Form\EventClassForm;
use Application\Form\EventClassFilter;
use Application\Form\MessageClassForm;
use Application\Form\MessageClassFilter;
use Application\Form\UserMessageClassForm;
use Application\Form\LoginForm;
use Application\Form\LoginFilter;
use Application\Model\User;
use Application\Model\EventClass;
use Application\Model\MessageClass;
use Application\Model\UserHasMessageClass;
use Application\Model\EventClassHasStore;
use Application\Model\MessageSpool;
use Application\Model\MessageArchive;
use Application\Model\EventArchive;

use DateTime;
class IndexController extends AbstractActionController
{
    protected $userTable;
    protected $eventClassTable;

    /*public function init()
    {
        $this->_authService = new Application\Service\Auth;
        $this->view->role = $this->_authService->getIdentity()->tipo;
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->flashMessenger = $this->_flashMessenger;
        $this->sessione = new Zend_Session_Namespace ( 'sessione' );
        //$translator=$this->getServiceLocator()->get('translator');



    } */

    public function indexAction()
    {
        $authService=$this->getServiceLocator()->get('AuthService');
        if ($authService->hasIdentity()) {
            return $this->redirect()->toRoute('home',array('action'=>'home'));
        }
        $form = new LoginForm();
        $form->setInputFilter(new LoginFilter());
        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost());
            if ($form->isValid()) {
                $authAdapter=$this->getServiceLocator()->get('Auth');
                $authAdapter->setIdentity($form->get('user_username')->getValue());
                $authAdapter->setCredential($form->get('user_password')->getValue());
                $result=$authService->authenticate($authAdapter);
                if (!$result->isValid()) {
                    $this->flashMessenger()->addMessage('Credenziali errate');
                    return $this->redirect()->toRoute('home', array('action' => 'index'));
                }
                else {
                    $userName=$authService->getIdentity();
                    $userDetails=$this->getTable('User')->getUserDetailByUsername($userName);
                    $authService->getStorage()->setRole($userDetails->user_type);
                    $authService->getStorage()->setUserId($userDetails->user_id);
                    return $this->redirect()->toRoute('home', array('action' => 'home'));
                }
            }
        }
        $this->layout('layout/layout_log-in.phtml');
        return array('form' => $form, 'messages'=> $this->flashMessenger()->getMessages());
    }

    public function logOutAction () {
        $authService=$this->getServiceLocator()->get('AuthService');
        $authService->clearIdentity();
        $this->flashMessenger()->addMessage('Utente disconnesso');
        return $this->redirect()->toRoute('home', array('action' => 'index'));
    }

    public function homeAction()
    {
        $authService=$this->getServiceLocator()->get('AuthService');
        $userId=$authService->getStorage()->getUserId();

        return array(
            'messages' => $this->flashMessenger()->getMessages(),
            'userId'=> $userId
        );
    }

    /**
     * Used for getting the table instance from Service Manager
     * @param $tableName
     * @return array|object
     */
    public function getTable($tableName)
    {
        //inizialization, just put for don't get IDE warning
        $riferimento=null;

        //$riferimento points to $this->$tableName.'Table'
        eval ("\$riferimento = &\$this->".$tableName."Table;");
        if (!$riferimento) {
            $sm = $this->getServiceLocator();
            $riferimento = $sm->get('Application\Model\DbTable\\'.$tableName.'Table');
        }
        return $riferimento;
    }

    public function userListAction()
    {
        /*return array('users' => $this->getUserTable()->fetchAll(),
            'messages' => $this->flashMessenger()->getMessages());
        */
        return array('users' => $this->getTable('User')->fetchAll(),
            'messages' => $this->flashMessenger()->getMessages());
    }
    public function userDetailAction()

    {
        $userId = $this->params('user_id');
        $userDetail=$this->getTable('User')->getUserDetailByUserId($userId);

        return array(
            'userDetail'=>$userDetail
        );

    }
    public function userAddAction()
    {
        $form = new UserFormSuper();
        $form->setInputFilter(new UserFilter());
        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost());
            //verifica che non ci sia già un utente con lo stesso username
            $form->getInputFilter()->add(array(
                'name' => 'user_username',
                'validators' => array(
                    array(
                        'name' => '\Zend\Validator\Db\NoRecordExists',
                        'options' => array(
                            'table' => 'user',
                            'field' => 'user_username',
                            'adapter' => $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')
                        ),
                    ),
                ),
            ));
            if ($form->isValid()) {
                $user = new User();
                $user->exchangeArray($form->getData());
                $user->user_password=md5($user->user_password);
                if ($this->getTable('User')->addUser($user)) {
                    $this->flashMessenger()->addMessage('Utente aggiunto correttamente');
                    return $this->redirect()->toRoute('users', array('action' => 'userList'));
                }
            }
        }
        return array('form' => $form);

    }

    public function userEditRegularAction () {
        $authService=$this->getServiceLocator()->get('AuthService');
        $userId=$authService->getStorage()->getUserId();
        $formHandling=$this->userUpdate($userId,new UserFormRegular());
        if ($formHandling===true) {
            $this->flashMessenger()->addMessage('Profilo modificato correttamente, si consiglia di rifare il login per rendere effettive tutte le impostazioni');
            return $this->redirect()->toRoute('home');
        }
        return array('form' => $formHandling);
    }

    public function userEditSuperAction()
    {
        $userId = $this->params('user_id');
        $formHandling=$this->userUpdate($userId,new UserFormSuper());
        if ($formHandling===true) {
            $this->flashMessenger()->addMessage('Utente modificato correttamente, si consiglia di rifare il login per rendere effettive tutte le impostazioni');
            return $this->redirect()->toRoute('users',array('action' => 'userList'));
        }
        return array('form' => $formHandling);
    }

    public function userUpdate ($user_id,$form) {
        $values = $this->getTable('User')->getUserDetailByUserId($user_id);
        $username_old = $values->user_username;
        //get_object_vars serve a convertire gli attributi dell'oggetto in array, populateValues accetta solo array
        $form->populateValues(get_object_vars($values));
        $form->setInputFilter(new UserFilter());
        $form->get('user_password')->setLabel('Password (inserirla solo se si vuole cambiarla)');
        //password facoltativa
        $form->getInputFilter()->get('user_password')->setRequired(false);
        $form->getInputFilter()->get('user_password_check')->setRequired(false);

        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost());
            //se ha scelto un diverso username
            if (strcmp($form->get('user_username')->getValue(), $username_old) != 0) {
                //verifica che non ci sia già un utente con lo stesso username
                $form->getInputFilter()->add(array(
                    'name' => 'user_username',
                    'validators' => array(
                        array(
                            'name' => '\Zend\Validator\Db\NoRecordExists',
                            'options' => array(
                                'table' => 'user',
                                'field' => 'user_username',
                                'adapter' => $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')
                            ),
                        ),
                    ),
                ));
            }
            //TODO Teoricamente se cambia lo smartphone type sarebbe opportuno eliminare il push token, ma comunque non dovrebbe dare problemi
            if (strlen($form->get('user_password')->getValue()) > 0) {
                $form->getInputFilter()->get('user_password_check')->setRequired(true);
            } else {
                $form->getInputFilter()->get('user_password_check')->setRequired(false);
            }
            if ($form->isValid()) {
                $user = new User();
                $user->exchangeArray($form->getData());
                if (strlen($user->user_password)!=0) {
                    $user->user_password=md5($user->user_password);
                }
                else {
                    unset ($user->user_password);
                }
                if (strlen($user->user_type)==0) {
                    unset ($user->user_type);
                }

                //if non messo perchè quando l'utente non cambia alcun campo l'update ritorna null
                //comunque i controlli vengono fatti dai validator, quindi è safe
                $this->getTable('User')->updateUser($user);
                return true;
            }
        }
        return $form;
    }

    public function userDelAction()
    {
        $userId = $this->params('user_id');
        if (!$userId) {
            $this->flashMessenger()->addMessage('Utente non specificato');
            return $this->redirect()->toRoute('home', array('action' => 'home'));
        }

        $this->getTable('User')->delUser($userId);
        $this->flashMessenger()->addMessage('Utente eliminato correttamente');
        return $this->redirect()->toRoute('users',array('action'=>'userList'));
    }

    public function setLayout1Column () {
        $this->layout('layout/layout_1_colonna.phtml');
    }




    public function eventClassAddAction()
    {
        $authService=$this->getServiceLocator()->get('AuthService');
        $role=$authService->getStorage()->getRole();
        $form = new EventClassForm();
        $form->setInputFilter(new EventClassFilter());
        $eventClassActive=new \Zend\Form\Element('event_class_active');
        $eventClassActive->setAttribute('type','hidden')
            ->setValue(true);
        $form->get('common')->add($eventClassActive);

        //get retail lists and populate the multicheckbox, columns: legacy_db_store_id, legacy_db_store_description, legacy_db_store_city, legacy_db_store_type
        $dbLegacy = new DbLegacy();
        $dbLegacy->connect();
        $risultati = $dbLegacy->query_exec('SELECT NGNEKNEG as legacy_db_store_id,NGNENOM as legacy_db_store_description,NGNELOC as legacy_db_store_city,NGNETIPO as legacy_db_store_type FROM LIBEMME.NGSENE WHERE NGNENOM<>\'\' ORDER BY legacy_db_store_description');
        $dbLegacy->close();
        foreach ($risultati as $risultatiElement) {
            $legacyDbStores[$risultatiElement['legacy_db_store_id']]=ucwords(strtolower($risultatiElement['legacy_db_store_description']));
        }
        $form->get('structured')->get('legacy_db_store_id')->setValueOptions($legacyDbStores);

        if ($role!=='developer') {
            $form->get('common')->get('event_class_type')->setValue('structured');
        }

        //valida solo il fieldset common
        $form->setValidationGroup('common');
        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost());

            //aggiunge la validazione anche per il fieldset della tipologia di evento selezionata (es. structured)
            //ciò inoltre comporta che il metodo $form->getData() fornisca solamente i valori degli elementi dei fieldset selezionati
            //così si evita di doverli impostare null a mano
            if ($event_class_type = $form->get('common')->get('event_class_type')->getValue()) {
                $form->setValidationGroup('common', $event_class_type);
            }

            //se non è stato selezionato neanche un negozio imposta 'tutti i negozi' come richiesto
            //(così poi il messaggio personalizzato compare sotto di esso anzichè sotto la lista dei negozi, serve ad evitare problemi col framework nel caso di multicheckbox)
            if (($event_class_type === 'structured') && ($form->get('structured')->get('legacy_db_store_id')->getValue() == null)) {
                $form->getInputFilter()->get('structured')->get('event_class_structured_all_stores')->setRequired(true);
            }

            //aggiunge validazione sul giusto campo di riferimento per il compared_query
            if (($event_class_type === 'compared_query') && ($form->get('compared_query')->get('event_class_compared_query_comparision_type')->getValue() === 'string_contained')) {
                $form->getInputFilter()->get('compared_query')->get('event_class_compared_query_string_reference_value')->setRequired(true);
            } else if ($event_class_type === 'compared_query') {
                $form->getInputFilter()->get('compared_query')->get('event_class_int_reference_value')->setRequired(true);
            }

            //aggiunge validazione sugli event_class_period giusti
            $event_class_period_type = $form->get('common')->get('event_class_period_type')->getValue();
            if ($event_class_period_type === 'days') {
                $form->getInputFilter()->get('common')->get('event_class_period_days')->setRequired(true);
            } else if ($event_class_period_type === 'dates') {
                $form->getInputFilter()->get('common')->get('event_class_period_date_start')->setRequired(true);
                $form->getInputFilter()->get('common')->get('event_class_period_date_end')->setRequired(true);
            }

            if ($form->isValid()) {
                $eventClass = new EventClass();
                $formData=$form->getData();
                //conversione data nel formato compatibile MySQL
                if ($formData['common']['event_class_period_type']=='dates') {
                    $dateStart=DateTime::createFromFormat('d/m/Y',$formData['common']['event_class_period_date_start']);
                    $formData['common']['event_class_period_date_start']=$dateStart->format('Y-m-d');
                    $dateEnd=DateTime::createFromFormat('d/m/Y',$formData['common']['event_class_period_date_end']);
                    $formData['common']['event_class_period_date_end']=$dateEnd->format('Y-m-d');
                }
                //l'eliminazione dei dati superflui (es. di un fieldset non di interesse) viene fatta nell'exchange array
                $eventClass->exchangeArray($formData);
                if ($eventClassId=$this->getTable('EventClass')->addEventClass($eventClass)) {
                    //se è stato selezionato un sottoinsieme di negozi
                    if (($event_class_type === 'structured') && ($form->get('structured')->get('event_class_structured_all_stores')->getValue() == false)) {
                        $storeIds=$form->get('structured')->get('legacy_db_store_id')->getValue();
                        foreach ($storeIds as $storeIdsElement) {
                            $eventClassHasStore=new EventClassHasStore();
                            $data=array(
                                'legacy_db_store_id' => $storeIdsElement,
                                'event_class_id' => $eventClassId
                            );
                            $eventClassHasStore->exchangeArray($data);
                            $this->getTable('EventClassHasStore')->addEventClassHasStore($eventClassHasStore);
                        }
                    }
                    $this->flashMessenger()->addMessage('Evento aggiunto correttamente');
                    return $this->redirect()->toRoute('events',array('action'=>'eventClassList'));
                }
                return true;
            }
        }
        return array('form' => $form);

    }

    public function eventClassDelAction()
    {
        $eventClassId = $this->params('event_class_id');
        if (!$eventClassId) {
            $this->flashMessenger()->addMessage('Evento non specificato');
            return $this->redirect()->toRoute('home', array('action' => 'home'));
        }

        $this->getTable('EventClass')->delElement($eventClassId);
        $this->flashMessenger()->addMessage('Evento eliminato correttamente');
        return $this->redirect()->toRoute('events',array('action'=>'eventClassList'));
    }

    public function eventClassResetAction()
    {
        $this->getTable('EventClass')->resetEvents();
        $this->flashMessenger()->addMessage('Eventi resettati correttamente');
        return $this->redirect()->toRoute('events',array('action'=>'eventClassList'));
    }

    public function eventArchiveListAction()
    {

        $result=$this->getTable('EventArchive')->getListWithEventsDetails();
        return array(
            'eventArchiveList'=>$result
        );
    }
    public function eventClassListAction()

    {
        $this->setLayout1Column();

        return array('events' => $this->getTable('EventClass')->fetchAllExisting(),
            'messages' => $this->flashMessenger()->getMessages());

    }
    public function eventClassDetailAction()

    {
        $eventClassId = $this->params('event_class_id');
        $eventClassDetail=$this->getTable('EventClass')->fetchAll($eventClassId)->current();
        $eventClassStoresIds=$this->getTable('EventClassHasStore')->getListByEventClass($eventClassId);
        $storesNames=array();

        if (($eventClassStoresIds->count()>0) || ($eventClassDetail->event_class_structured_all_stores)) {
            $dbLegacy=new DbLegacy();
            $dbLegacy->connect();
            $storesList=$dbLegacy->getStoresList();
            $dbLegacy->close();

            if ($eventClassDetail->event_class_structured_all_stores) {
                foreach ($storesList as $storesListEl) {
                    $storesNames[]=$storesListEl['legacy_db_store_description'];
                }
            }
            else {
                foreach ($eventClassStoresIds as $eventClassStoresIdsEl) {
                    foreach ($storesList as $storesListEl) {
                        if ($storesListEl['legacy_db_store_id']===$eventClassStoresIdsEl->legacy_db_store_id) {
                            $storesNames[]=$storesListEl['legacy_db_store_description'];
                            break;
                        }
                    }
                }
            }
        }

        return array(
            'eventClassDetail'=>$eventClassDetail,
            'storesNames'=>$storesNames
        );

    }

    public function messageClassListAction (){
        $eventClassId = $this->params('event_class_id');
        $authService=$this->getServiceLocator()->get('AuthService');
        $userId=$authService->getStorage()->getUserId();
        $role=$authService->getStorage()->getRole();
        $dataArray=array(
            'role'=>$role,
            'messages' => $this->getTable('MessageClass')->fetchAll($eventClassId),
            'flashMessages' => $this->flashMessenger()->getMessages()
        );

        $dataArray['messages']->buffer();
        $messageClassList=$dataArray['messages'];
        for ($i=0;$i<$messageClassList->count();$i++) {
            $form = new UserMessageClassForm('form'.($i+1));
            $messageClassId=$messageClassList->current()->message_class_id;
            $form->get('message_class_id')->setValue($messageClassId);
            $resultSet=$this->getTable('UserHasMessageClass')->fetchAll($userId,$messageClassId);
            if ($resultSet->count()!=0) {
                $userHasMessageClassInterval=$resultSet->current()->user_has_message_class_interval;
                $form->get('user_has_message_class_interval')->setValue($userHasMessageClassInterval);
            }
            else {
                //Non iscritto all'evento
                $form->get('user_has_message_class_interval')->setValue('999');
            }
            $dataArray['formList'][]=$form;
            $messageClassList->next();
        }

        if ($eventClassId) {
            $dataArray['eventClass']=$this->getTable('EventClass')->fetchAllExisting($eventClassId);
        }
        return $dataArray;
    }

    /**
     * Add a message class
     * @return \Zend\Http\Response
     */
    public function messageClassAddAction()
    {
        $eventClassId = $this->params('event_class_id');
        if ($eventClassId) {
            $dataArray['eventClass']=$this->getTable('EventClass')->fetchAllExisting($eventClassId);
        }
        $form = new MessageClassForm();
        $form->get('event_class_id')->setValue($eventClassId);
        $form->setInputFilter(new MessageClassFilter());
        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost());

            if ($form->isValid()) {
                $messageClass = new MessageClass();
                $formData=$form->getData();
                $messageClass->exchangeArray($formData);
                if ($messageClassId=$this->getTable('MessageClass')->addMessageClass($messageClass)) {
                    $this->flashMessenger()->addMessage('Messaggio aggiunto correttamente');
                    return $this->redirect()->toRoute('messages/wildcard', array('action' => 'messageClassList','event_class_id'=>$formData['event_class_id'],'message_class_id'=>$messageClassId));
                }
            }
        }
        $dataArray['form']=$form;
        return $dataArray;

    }

    public function messageClassEditAction()
    {
        $messageClassId = $this->params('message_class_id');
        $eventClassId = $this->params('event_class_id');
        if (!$messageClassId) {
            $this->flashMessenger()->addMessage('Messaggio non specificato');
            return $this->redirect()->toRoute('home', array('action' => 'home'));
        }
        $messageClassDetails=$this->getTable('MessageClass')->getElement($messageClassId);
        $form = new MessageClassForm();
        $form->setInputFilter(new MessageClassFilter());
        $form->populateValues(get_object_vars($messageClassDetails));

        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost());

            if ($form->isValid()) {
                $messageClass = new MessageClass();
                $formData=$form->getData();
                $messageClass->exchangeArray($formData);
                if ($messageClassId=$this->getTable('MessageClass')->EditMessageClass($messageClass,$messageClassId)) {
                    $this->flashMessenger()->addMessage('Messaggio modificato correttamente');
                    return $this->redirect()->toRoute('messages/wildcard', array('action' => 'messageClassList','event_class_id'=>$formData['event_class_id']));
                }
            }
        }
        $dataArray['form']=$form;
        $dataArray['messageClassId']=$messageClassId;
        $dataArray['eventClassId']=$eventClassId;
        return $dataArray;
    }

    public function messageClassDelAction()
    {
        $messageClassId = $this->params('message_class_id');
        if (!$messageClassId) {
            $this->flashMessenger()->addMessage('Messaggio non specificato');
            return $this->redirect()->toRoute('home', array('action' => 'home'));
        }
        $eventClassId = $this->params('event_class_id');

        $this->getTable('MessageClass')->delElement($messageClassId);
        $this->flashMessenger()->addMessage('Messaggio eliminato correttamente');
        return $this->redirect()->toRoute('messages/wildcard', array('action' => 'messageClassList','event_class_id'=>$eventClassId));
    }

    public function messageArchiveListAction () {
        $authService=$this->getServiceLocator()->get('AuthService');
        $userId=$authService->getStorage()->getUserId();
        $messages=$this->getTable('MessageArchive')->getMessages($userId);
        return array(
            'archiveMessages'=>$messages
        );
    }

    public function systemMessageArchiveListAction () {
        $messages=$this->getTable('MessageArchive')->getMessages();
        return array(
            'archiveMessages'=>$messages
        );
    }

    public function systemMessageSpoolListAction () {
        $messages=$this->getTable('MessageSpool')->getListWithUsersDetails();
        return array(
            'archiveMessages'=>$messages
        );
    }

    public function messageArchiveDeleteAjaxAction () {
        $authService=$this->getServiceLocator()->get('AuthService');
        $userId=$authService->getStorage()->getUserId();
        $messageArchiveId = $this->params()->fromPost('message_archive_id');
        $status='ko';
        if (!$messageArchiveId) {
            $jsonObject=\Zend\Json\Json::encode(array(
                'status'=>$status
            ));
            return $this->getResponse()->setContent($jsonObject);
        }
        //delete
        if ($this->getTable('MessageArchive')->deleteMessage($userId,$messageArchiveId)) {
            $status='ok';
        }

        $jsonObject=\Zend\Json\Json::encode(array(
            'status'=>$status
        ));
        return $this->getResponse()->setContent($jsonObject);


    }


    /**
     * User subscribes to a message class
     * @return mixed
     */
    public function userAddMessageClassAjaxAction () {
        $authService=$this->getServiceLocator()->get('AuthService');
        $userId=$authService->getStorage()->getUserId();
        $messageClassId = $this->params()->fromPost('message_class_id');
        $userHasMessageClassInterval=$this->params()->fromPost('user_has_message_class_interval');

        $status='ko';
        //delete
        if (($userHasMessageClassInterval==999)&&($this->getTable('UserHasMessageClass')->deleteElement($userId,$messageClassId))) {
            $status='ok';
        }
        else if ($userHasMessageClassInterval!=999){
            $resultSet=$this->getTable('UserHasMessageClass')->fetchAll($userId,$messageClassId);
            $dati=new UserHasMessageClass();
            $dateTime=new DateTime();
            $dateTime->setTimezone(new \DateTimeZone('Europe/Rome'));
            $dati->exchangeArray(array(
                'user_id'=>$userId,
                'message_class_id'=>$messageClassId,
                'user_has_message_class_interval'=>$userHasMessageClassInterval,
                'user_has_message_class_last_send'=> $dateTime->format('Y-m-d G:i:s')
            ));
            //record non già presente
            if (($resultSet->count()==0)&&($this->getTable('UserHasMessageClass')->addElement($dati))) {
                $status='ok';
            }
            //record presente
            else if (($resultSet->count()>0) &&($this->getTable('UserHasMessageClass')->updateElement($dati))){
                $status='ok';
            }
        }
        $jsonObject=\Zend\Json\Json::encode(array(
            'status'=>$status
        ));
        return $this->getResponse()->setContent($jsonObject);


    }

    public function generaVarioCodiceAction()
    {
        //la si genera tramite sql workbench andando su plugins-objects
        $stringa_colonneUnderscore = 'message_archive_id, message_archive_content, message_archive_datetime_sent, user_id';
        $colonneUnderscore = explode(', ', $stringa_colonneUnderscore);

        //le si crea tramite replace in un editor di testo, risultato es. eventCass_check_interval
        //non so a cosa serva, comunque se serve basta cambiare la var nel return array
        $stringa_colonneCamelCase = "legacyDb_store_id, eventClass_id";
        $colonneCamelCase = explode(', ', $stringa_colonneCamelCase);

        foreach ($colonneUnderscore as $colonneUnderscoreElement) {
            $flag=0;
            $colonneRealmenteCamelCaseElement=null;
            $parole=explode('_',$colonneUnderscoreElement);
            foreach ($parole as $paroleElement) {
                if ($flag==0) {
                    $colonneRealmenteCamelCaseElement=$paroleElement;
                    $flag=1;
                }
                else {
                    $colonneRealmenteCamelCaseElement=$colonneRealmenteCamelCaseElement.ucfirst($paroleElement);
                }
            }
            $colonneRealmenteCamelCase[]=$colonneRealmenteCamelCaseElement;
            $flag=0;
        }

        $stringa_tabelle='user, event_class, message_class, user_has_message_class, event_class_has_store, message_spool, message_archive, event_archive';
        $tabelleUnderscore=explode(', ', $stringa_tabelle);
        foreach ($tabelleUnderscore as $tabelleUnderscoreElement) {
            $tabelleCamelCaseElement=null;
            $parole=explode('_',$tabelleUnderscoreElement);
            foreach ($parole as $paroleElement) {
                $tabelleCamelCaseElement=$tabelleCamelCaseElement.ucfirst($paroleElement);
            }
            $tabelleCamelCase[]=$tabelleCamelCaseElement;
        }


        $varName = 'eventsElement';
        return array(
            'colonneCamelCase' => $colonneUnderscore,
            'colonneRealmenteCamelCase' => $colonneRealmenteCamelCase,
            'colonneUnderscore' => $colonneUnderscore,
            'tabelleUnderscore' => $tabelleUnderscore,
            'tabelleCamelCase' => $tabelleCamelCase,
            'varName' => $varName
        );
    }
}

