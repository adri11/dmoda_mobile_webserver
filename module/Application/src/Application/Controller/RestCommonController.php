<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractRestfulController;
//use Application\Service\Auth;
use Application\Model\DbLegacy;
use Application\Form\UserForm;
use Application\Form\UserFilter;
use Application\Form\EventClassForm;
use Application\Form\EventClassFilter;
use Application\Form\MessageClassForm;
use Application\Form\MessageClassFilter;
use Application\Form\UserMessageClassForm;
use Application\Model\User;
use Application\Model\EventClass;
use Application\Model\MessageClass;
use Application\Model\UserHasMessageClass;
use Application\Model\EventClassHasStore;
use Application\Model\MessageSpool;
use Application\Model\MessageArchive;
use Application\Model\EventArchive;
use Zend\View\Model\JsonModel;

use DateTime;

class RestCommonController extends AbstractRestfulController {

    public function returnJsonWithHeader (JsonModel $jsonData) {
        return $this->getResponseWithHeader()->setContent($jsonData->serialize());
    }

    public function returnError ($text='error') {
        $jsonModel=new JsonModel(array(
            'status' => 'error',
            'text' => $text
        ));
        return $this->getResponseWithHeader()->setContent($jsonModel->serialize());
    }
    // configure response
    public function getResponseWithHeader()
    {
        $response = $this->getResponse();
        $response->getHeaders()
            //make can accessed by *
            ->addHeaderLine('Access-Control-Allow-Origin','*')
            //set allow methods
            ->addHeaderLine('Access-Control-Allow-Methods','POST PUT DELETE GET');

        return $response;
    }

    public function get($id)
    {

    }

    public function getList()
    {

    }


    public function create($data)
    {

    }


    public function update($id, $data)
    {

    }

    public function delete($id)
    {

    }

    //used for Chrome, cause sometimes it send OPTION request before the requested method
    public function options()
    {
        $response = $this->getResponse();
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Access-Control-Allow-Origin: *');
        $headers->addHeaderLine('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        return $response;

    }


}

