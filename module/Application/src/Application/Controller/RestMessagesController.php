<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractRestfulController;
//use Application\Service\Auth;
use Application\Model\DbLegacy;
use Application\Form\UserForm;
use Application\Form\UserFilter;
use Application\Form\EventClassForm;
use Application\Form\EventClassFilter;
use Application\Form\MessageClassForm;
use Application\Form\MessageClassFilter;
use Application\Form\UserMessageClassForm;
use Application\Model\User;
use Application\Model\EventClass;
use Application\Model\MessageClass;
use Application\Model\UserHasMessageClass;
use Application\Model\EventClassHasStore;
use Application\Model\MessageSpool;
use Application\Model\MessageArchive;
use Application\Model\EventArchive;
use Zend\View\Model\JsonModel;

use DateTime;

/**
 * Handling of message exchange (view, viewed notification,deletion)
 * To be used it requires the communication of the Rest Token
 *
 * @package Application\Controller
 */
class RestMessagesController extends RestCommonController {

    public function getTable($tableName)
    {
        //inizialization, just put for don't get IDE warning
        $riferimento=null;

        //$riferimento points to $this->$tableName.'Table'
        eval ("\$riferimento = &\$this->".$tableName."Table;");
        if (!$riferimento) {
            $sm = $this->getServiceLocator();
            $riferimento = $sm->get('Application\Model\DbTable\\'.$tableName.'Table');
        }
        return $riferimento;
    }

    /**
     * Return all the messages for a user
     * @param mixed $id The user id
     * @return mixed|\Zend\View\Model\JsonModel
     */
    //testato
    public function get($id)
    {

        $dateTime=new DateTime();
        $dateTime->setTimezone(new \DateTimeZone('Europe/Rome'));
        $datiArray=array();

        $restToken=$id;
        $userDetail=$this->getTable('user')->getUserDetailByRestToken($restToken);

        if ($userDetail==null) {
            return $this->returnError('Rest token not valid');
        }

        $userId=$userDetail->user_id;
        /*
         * TODO Valuta se farla con transazione, cioè il trasferimento a messageArchive dev'essere
         fatta solo in seguito alla conferma della ricezione da parte dell'utente
        $sm=$this->getServiceLocator();
        $adapter=$sm->get('Zend\Db\Adapter\Adapter');
        $adapter->getDriver()->getConnection()->beginTransaction();*/

        $pendingMessages=$this->getTable('message_spool')->getPendingMessages($userId);
        $oldMessages=$this->getTable('message_archive')->getMessages($userId);

        //moves pending messages in messageArchive table and prepare the array to be returned

        foreach ($pendingMessages as $pendingMessagesElement) {
            $messageArchiveObject=new MessageArchive();
            $messageArchiveObject->exchangeArray(array(
                'message_archive_content'=>$pendingMessagesElement['message_spool_content'],
                'user_id'=>$pendingMessagesElement['user_id'],
                'message_archive_datetime_sent'=>$dateTime->format('Y-m-d G:i:s')
            ));
            $messageArchiveId=$this->getTable('message_archive')->addMessage($messageArchiveObject);

            //TODO ricevere prima comunicazione di ricezione da parte del client?
            $this->getTable('message_spool')->deleteMessage($pendingMessagesElement['message_spool_id']);

            $messageArchiveArray=get_object_vars($messageArchiveObject);
            $messageArchiveArray['message_archive_id']=$messageArchiveId;
            $messageArchiveArray['newMessage']=true;
            $datiArray[]=$messageArchiveArray;
        }
        foreach ($oldMessages as $oldMessagesElement) {
            $datiArray[]=$oldMessagesElement;
        }
        $modello=new JsonModel($datiArray);
        return $this->getResponseWithHeader()->setContent($modello->serialize());
        //return $modello;
    }

    public function getList()
    {
        return $this->returnError('Not allowed');
    }


    public function create($data)
    {
        return $this->returnError('Not allowed');
    }


    public function update($id, $data)
    {
        return $this->returnError('Not allowed');
    }

    /**
     * User deletes a message
     * @param mixed $id message_id
     * @return mixed
     */

    public function delete($id)
    {
        $messageArchiveId=$id;
        $restToken=$this->params()->fromQuery('rest-token');
        $userElement=$this->getTable('user')->getUserDetailByRestToken($restToken);
        if ($userElement==null) {
            return $this->returnError('Wrong credentials delete '.$id.' decode:'.$restToken);
        }
        $this->getTable('messageArchive')->deleteMessage($userElement->user_id,$messageArchiveId);
        $jsonModel=new JsonModel(array(
            'status'=>'ok'
        ));
        return $this->returnJsonWithHeader($jsonModel);
    }
}

