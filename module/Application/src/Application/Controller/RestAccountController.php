<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractRestfulController;
//use Application\Service\Auth;
use Application\Model\DbLegacy;
use Application\Form\UserForm;
use Application\Form\UserFilter;
use Application\Form\EventClassForm;
use Application\Form\EventClassFilter;
use Application\Form\MessageClassForm;
use Application\Form\MessageClassFilter;
use Application\Form\UserMessageClassForm;
use Application\Model\User;
use Application\Model\EventClass;
use Application\Model\MessageClass;
use Application\Model\UserHasMessageClass;
use Application\Model\EventClassHasStore;
use Application\Model\MessageSpool;
use Application\Model\MessageArchive;
use Application\Model\EventArchive;
use Zend\View\Model\JsonModel;

use DateTime;
class RestAccountController extends RestCommonController {

    public function getTable($tableName)
    {
        //inizialization, just put for don't get IDE warning
        $riferimento=null;

        //$riferimento points to $this->$tableName.'Table'
        eval ("\$riferimento = &\$this->".$tableName."Table;");
        if (!$riferimento) {
            $sm = $this->getServiceLocator();
            $riferimento = $sm->get('Application\Model\DbTable\\'.$tableName.'Table');
        }
        return $riferimento;
    }

    public function get($id)
    {
        return $this->returnError('Method not allowed');
    }

    public function getList()
    {
        return $this->returnError('Method not allowed');
    }

    /**
     * Log-in
     * @param mixed $data User, password and push token
     * @return mixed Rest token
     */
    //testato
    public function create($data)
    {
        if ((!isset($data['user']))||(!isset ($data['pwd']))) {
            return $this->returnError('Not specified credentials');
        }
        $userElement=$this->getTable('user')->getUserDetailByUsernameAndPassword($data['user'],md5($data['pwd']));
        if ($userElement==null) {
            return $this->returnError('Wrong credentials');
        }
        if (isset($data['push-token'])) {
            $userElement->user_smartphone_token=$data['push-token'];
        }
        //TESI Come suggerito qui, se si cerca la parola "token": http://www.php.net/manual/en/function.uniqid.php
        //Soluzione ignorata perchè introduce il simbolo + che ha effetti disastrosi nelle chiamate REST (anche usando l'encoding)
        //$restToken = base64_encode( openssl_random_pseudo_bytes(100));
        $restToken =bin2hex(openssl_random_pseudo_bytes(50));
        $userElement->user_rest_token=$restToken;
        $jsonModel=new JsonModel(array(
            'rest-token' =>$restToken
        ));
        $this->getTable('user')->updateUser($userElement);
        return $this->returnJsonWithHeader($jsonModel);
    }

    /**
     * Update of the push token
     * @param mixed $id Rest-token
     * @param mixed $data
     * @return mixed
     */
    //testato
    public function update($id, $data)
    {
        $restToken=$id;
        $userElement=$this->getTable('user')->getUserDetailByRestToken($restToken);
        if ($userElement==null) {
            return $this->returnError('Wrong credentials update');
        }
        if (isset($data['push-token'])) {
            $userElement->user_smartphone_token=$data['push-token'];
            $this->getTable('user')->updateUser($userElement);
            $jsonModel=new JsonModel(array(
                'status'=>'ok'
            ));
            return $this->returnJsonWithHeader($jsonModel);
        }
        else {
            return $this->returnError('Push token not specified');
        }
    }

    //testato
    public function replaceList($data) {
        return $this->returnError('Not specified credentials');
    }

    /**
     * Log-out
     * @param mixed $id Rest token
     * @return mixed
     */
    //testato
    public function delete($id)
    {
        $restToken=$id;
        $userElement=$this->getTable('user')->getUserDetailByRestToken($restToken);
        if ($userElement==null) {
            return $this->returnError('Wrong credentials delete '.$id.' decode:'.$restToken);
        }
        $userElement->user_rest_token=null;
        $userElement->user_smartphone_token=null;
        $this->getTable('user')->updateUser($userElement);
        $jsonModel=new JsonModel(array(
            'status'=>'ok'
        ));
        return $this->returnJsonWithHeader($jsonModel);
    }

    public function deleteList() {
        return $this->returnError('Not allowed');
    }

}

