<?php
namespace Application\Model;

class UserHasMessageClass
{
    public $user_id;
    public $message_class_id;
    public $user_has_message_class_interval;
    public $user_has_message_class_last_send;
    public function exchangeArray($data) {
        $this->user_id=(isset($data['user_id']))?$data['user_id']:null;
        $this->message_class_id=(isset($data['message_class_id']))?$data['message_class_id']:null;
        $this->user_has_message_class_interval=(isset($data['user_has_message_class_interval']))?$data['user_has_message_class_interval']:null;
        $this->user_has_message_class_last_send=(isset($data['user_has_message_class_last_send']))?$data['user_has_message_class_last_send']:null;
    }


}

?>
