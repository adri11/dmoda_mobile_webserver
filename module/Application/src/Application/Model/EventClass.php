<?php
namespace Application\Model;
use DateTime;

class EventClass
{
    public $event_class_id;
    public $event_class_name;
    public $event_class_description;
    public $event_class_type;
    public $event_class_check_interval;
    public $event_class_int_reference_value;
    public $event_class_period_type;
    public $event_class_period_date_start;
    public $event_class_period_date_end;
    public $event_class_period_days;
    public $event_class_structured_comparision_type;
    public $event_class_structured_value_type;
    public $event_class_structured_all_stores;
    public $event_class_query;
    public $event_class_compared_query_comparision_type;
    public $event_class_compared_query_string_reference_value;
    public $event_class_delta_counter;
    public $event_class_target_reached;
    public $event_class_last_period_current_start;
    public $event_class_last_check;
    public $event_class_last_query_result;
    public $event_class_active;
    public $event_class_deleted;

    public function exchangeArray($data)
    {
        // ******** COMMON FIELDSET *****
        $common=isset($data['common'])?$data['common']:$data;
        $this->event_class_id = (isset($common['event_class_id'])) ? $common['event_class_id'] : null;
        $this->event_class_name = (isset($common['event_class_name'])) ? $common['event_class_name'] : null;
        $this->event_class_description = (isset($common['event_class_description'])) ? $common['event_class_description'] : null;
        $this->event_class_check_interval = (isset($common['event_class_check_interval'])) ? $common['event_class_check_interval'] : null;

        $this->event_class_period_type = (isset($common['event_class_period_type'])) ? $common['event_class_period_type'] : null;

        if ($this->event_class_period_type==='days') {
            $this->event_class_period_days = (isset($common['event_class_period_days'])) ? $common['event_class_period_days'] : null;
            $this->event_class_period_date_start=null;
            $this->event_class_period_date_end=null;
        }
        else if ($this->event_class_period_type==='dates') {
            $this->event_class_period_date_start = (isset($common['event_class_period_date_start'])) ? $common['event_class_period_date_start'] : null;
            $this->event_class_period_date_end = (isset($common['event_class_period_date_end'])) ? $common['event_class_period_date_end'] : null;
            $this->event_class_period_days = null;
        }

        $this->event_class_type = (isset($common['event_class_type'])) ? $common['event_class_type'] : null;

        //************ STRUCTURED FIELDSET **********************
        $structured=isset($data['structured'])?$data['structured']:$data;
            $this->event_class_int_reference_value = (isset($structured['event_class_int_reference_value'])) ? $structured['event_class_int_reference_value']: $this->event_class_int_reference_value;

            $this->event_class_structured_comparision_type = (isset($structured['event_class_structured_comparision_type'])) ? $structured['event_class_structured_comparision_type'] : null;
            $this->event_class_structured_all_stores = (isset($structured['event_class_structured_all_stores'])) ? $structured['event_class_structured_all_stores'] : null;
            $this->event_class_structured_value_type = (isset($structured['event_class_structured_value_type'])) ? $structured['event_class_structured_value_type'] : null;

        //*********** COMPARED QUERY FIELDSET *****************

        $comparedQuery=isset($data['compared_query'])?$data['compared_query']:$data;

        if ((isset($comparedQuery['event_class_string_reference_value'])&&($comparedQuery['event_class_compared_query_comparision_type']==='string_contained'))) {
            $this->event_class_compared_query_string_reference_value = (isset($comparedQuery['event_class_compared_query_string_reference_value'])) ? $comparedQuery['event_class_compared_query_string_reference_value'] : null;
            $this->event_class_int_reference_value = null;
        }
        else if (isset($comparedQuery['event_class_compared_query_comparision_type'])) {
            $this->event_class_int_reference_value = (isset($comparedQuery['event_class_int_reference_value'])) ? $comparedQuery['event_class_int_reference_value'] : null;
            $this->event_class_compared_query_string_reference_value =null;
        }
        else {
            $this->event_class_compared_query_string_reference_value =null;
        }

        $this->event_class_query = (isset($comparedQuery['event_class_query'])) ? $comparedQuery['event_class_query'] : $this->event_class_query;
        $this->event_class_compared_query_comparision_type = (isset($comparedQuery['event_class_compared_query_comparision_type'])) ? $comparedQuery['event_class_compared_query_comparision_type'] : null;


        //************** RAW QUERY FIELDSET *******************

        $rawQuery=isset($data['raw_query'])?$data['raw_query']:$data;

        $this->event_class_query = (isset($rawQuery['event_class_query'])) ? $rawQuery['event_class_query'] : $this->event_class_query;

        //************** LOGIC VARIABLES **********************

        $this->event_class_delta_counter = (isset($data['event_class_delta_counter'])) ? $data['event_class_delta_counter'] : null;
        $this->event_class_target_reached = (isset($data['event_class_target_reached'])) ? $data['event_class_target_reached'] : false;
        $currentDate=new DateTime();
        $this->event_class_last_period_current_start = (isset($data['event_class_last_period_current_start'])) ? $data['event_class_last_period_current_start'] : $currentDate->format("Y-m-d");
        $this->event_class_last_check = (isset($data['event_class_last_check'])) ? $data['event_class_last_check'] : null;
        $this->event_class_last_query_result = (isset($data['event_class_last_query_result'])) ? $data['event_class_last_query_result'] : null;
        $this->event_class_active = (isset($common['event_class_active'])) ? $common['event_class_active'] : false;
        $this->event_class_deleted = (isset($common['event_class_deleted'])) ? $common['event_class_deleted'] : false;
    }
}

?>