<?php
namespace Application\Model;


class User
{
    public $user_id;
    public $user_username;
    public $user_password;
    public $user_description;
    public $user_smartphone_token;
    public $user_smartphone_type;
    public $user_type;
    public $user_deleted;
    public $user_rest_token;


    public function exchangeArray ($data){
        $this->user_id=(isset($data['user_id']))?$data['user_id']:null;
        $this->user_username=(isset($data['user_username']))?$data['user_username']:null;
        $this->user_password=(isset($data['user_password']))?$data['user_password']:null;
        $this->user_description=(isset($data['user_description']))?$data['user_description']:null;
        $this->user_smartphone_token=(isset($data['user_smartphone_token']))?$data['user_smartphone_token']:null;
        $this->user_smartphone_type=(isset($data['user_smartphone_type']))?$data['user_smartphone_type']:null;
        $this->user_type=(isset($data['user_type']))?$data['user_type']:null;
        $this->user_deleted=(isset($data['user_deleted']))?$data['user_deleted']:null;
        $this->user_rest_token=(isset($data['user_rest_token']))?$data['user_rest_token']:null;
    }

}
?>