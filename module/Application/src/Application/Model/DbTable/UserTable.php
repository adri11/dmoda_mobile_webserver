<?php
namespace Application\Model\DbTable;

use Zend\Db\TableGateway\TableGateway;
use Application\Model\User;
use Zend\Db\Sql\Sql;

class UserTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter=$this->tableGateway->getAdapter();
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }

    public function getUserDetailByUserId ($id) {
        $resultSet=$this->tableGateway->select(array('user_id'=>$id));
        return $resultSet->current();
    }
    public function getUserDetailByRestToken ($restToken) {
        $resultSet=$this->tableGateway->select(array('user_rest_token'=>$restToken));
        return $resultSet->current();
    }
    public function getUserDetailByUsernameAndPassword ($username,$password) {
        $resultSet=$this->tableGateway->select(array(
            'user_username'=>$username,
            'user_password'=>$password
        ));
        $current=$resultSet->current();
        if ($current==null) {
            return null;
        }
        else {
            return $current;
        }
    }
    public function getUserDetailByUsername ($username) {
        $resultSet=$this->tableGateway->select(array('user_username'=>$username));
        return $resultSet->current();
    }
    public function addUser (User $data) {
        $data=get_object_vars($data);
        return $this->tableGateway->insert($data);
    }
    public function updateUser (User $data) {
        $data=get_object_vars($data);

        //ritorna le righe che sono state modificate, se la riga non è cambiata in seguito all'update ritorna null, ATTENZIONE.!
        return $this->tableGateway->update($data,array('user_id'=>$data['user_id']));
    }
    public function delUser ($userId) {
        $sql=new Sql($this->adapter);

        $delete=$sql->delete()
            ->from('user_has_message_class')
            ->where(array(
                'user_id'=>$userId
            ));
        $statement=$sql->prepareStatementForSqlObject($delete);
        $statement->execute();

        $delete=$sql->delete()
            ->from('message_spool')
            ->where(array(
                'user_id'=>$userId
            ));
        $statement=$sql->prepareStatementForSqlObject($delete);
        $statement->execute();

        $delete=$sql->delete()
            ->from('message_archive')
            ->where(array(
                'user_id'=>$userId
            ));
        $statement=$sql->prepareStatementForSqlObject($delete);
        $statement->execute();

        return $this->tableGateway->delete(array('user_id'=>$userId));
    }
}
?>