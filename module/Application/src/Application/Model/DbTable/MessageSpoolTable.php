<?php
namespace Application\Model\DbTable;

use Zend\Db\TableGateway\TableGateway;
use Application\Model\MessageSpool;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Sql;

class MessageSpoolTable extends AbstractTableGateway
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter=$this->tableGateway->getAdapter();
        $this->table=$this->tableGateway->getTable();
        $this->initialize();
    }


    public function fetchAll($userId=null)
    {
        $params=array();
        if (isset($userId)) {
            $params['user_id']=$userId;
        }
        $resultSet = $this->tableGateway->select($params);
        return $resultSet;
    }

    /**
     * get the pending messages for a specific user (the ones that have programmed_delivery already expired)
     * @param null $userId
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    public function getPendingMessages ($userId=null) {

        $sql=new Sql ($this->adapter);
        $select=$sql->select()
            ->from($this->table)
            ->where('message_spool_programmed_delivery < NOW()')
            ->columns(array('message_spool_id','message_spool_content','message_spool_programmed_delivery','user_id'));

        if (isset($userId)) {
            $select->where(array(
                'user_id'=>$userId
            ));
        }

        $statement=$sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function getListWithUsersDetails () {

        $sql=new Sql ($this->adapter);
        $select=$sql->select()
            ->from($this->table)
            ->join('user','message_spool.user_id=user.user_id')
            ->order(array(
                'message_spool_programmed_delivery DESC'
            ));


        $statement=$sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function deleteMessage ($messageSpoolId=null)
    {
        if ($this->tableGateway->delete(array('message_spool_id'=>$messageSpoolId))) {
            return true;
        }
        else {
            return null;
        }
    }
}
