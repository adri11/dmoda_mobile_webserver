<?php
namespace Application\Model\DbTable;

use Zend\Db\TableGateway\TableGateway;
use Application\Model\EventClass;
use Zend\Db\Sql\Sql;
use DateTime;

class EventClassTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter=$this->tableGateway->getAdapter();
    }

    public function fetchAll($eventClassId=null)
    {
        $whereArray=null;
        if ($eventClassId) {
            $whereArray=array('event_class_id'=>$eventClassId);
        }
        $resultSet = $this->tableGateway->select($whereArray);
        return $resultSet;
    }



    public function fetchAllExisting($eventClassId=null)
    {
        $whereArray=array('event_class_deleted'=>false);

        if ($eventClassId) {
            $whereArray['event_class_id']=$eventClassId;
        }

        $resultSet = $this->tableGateway->select($whereArray);
        return $resultSet;
    }

    public function addEventClass (EventClass $data) {
        $data=get_object_vars($data);
        if ($this->tableGateway->insert($data)) {
            //returns the id assigned to the inserted row
            return $this->tableGateway->lastInsertValue;
        }
        else {
            return null;
        }
    }

    public function delElement ($eventClassId) {

        $sql = new Sql ($this->adapter);
        $update = $sql->update('event_class')
            ->set(array(
                'event_class_deleted' => true
            ))
            ->where(array(
                'event_class_id' => $eventClassId
            ));
        $statement = $sql->prepareStatementForSqlObject($update);
        $statement->execute();
        $this->adapter->query("SET foreign_key_checks = 0", \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);

        $query="DELETE message_class, user_has_message_class FROM message_class LEFT JOIN user_has_message_class ON message_class.message_class_id=user_has_message_class.message_class_id WHERE event_class_id=".$eventClassId;
        $result = $this->adapter->query($query, \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);

        $this->adapter->query("SET foreign_key_checks = 1", \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);


    }
    public function resetEvents() {
        $currentDate=new DateTime();

        $sql = new Sql ($this->adapter);
        $update = $sql->update('event_class')
            ->set(array(
                'event_class_delta_counter' => null,
                'event_class_target_reached' => 0,
                'event_class_last_period_current_start' => $currentDate->format("Y-m-d"),
                'event_class_last_check' => null,
                'event_class_last_query_result' =>null ,
                'event_class_active' => 1,

            ));
        $statement = $sql->prepareStatementForSqlObject($update);
        $statement->execute();

        $update = $sql->update('user_has_message_class')
            ->set(array(
                'user_has_message_class_last_send' => $currentDate->format('Y-m-d G:i:s')
            ));
        $statement = $sql->prepareStatementForSqlObject($update);
        $statement->execute();
    }

}
?>