<?php
namespace Application\Model\DbTable;

use Zend\Db\TableGateway\TableGateway;
use Application\Model\UserHasMessageClass;

class UserHasMessageClassTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }


    public function fetchAll($userId=null,$messageClassId=null)
    {
        $params=array();
        if (isset($userId)) {
            $params['user_id']=$userId;
        }
        if (isset($messageClassId)) {
            $params['message_class_id']=$messageClassId;
        }
        $resultSet = $this->tableGateway->select($params);
        return $resultSet;
    }

    public function addElement (UserHasMessageClass $data) {
        $data=get_object_vars($data);
        if ($this->tableGateway->insert($data)) {
            //returns the id assigned to the inserted row
            return true;
        }
        else {
            return null;
        }
    }

    public function addElementArray ($data) {
        if ($this->tableGateway->insert($data)) {
            //returns the id assigned to the inserted row
            return true;
        }
        else {
            return null;
        }
    }

    public function updateElement (UserHasMessageClass $data) {
        $data=get_object_vars($data);
        if ($this->tableGateway->update($data,array(
            'user_id'=>$data['user_id'],
            'message_class_id'=>$data['message_class_id']
        ))) {
            return true;
        }
        else {
            return null;
        }
    }

    public function deleteElement ($userId=null,$messageClassId=null)
    {
        $params=array();
        if (isset($userId)) {
            $params['user_id']=$userId;
        }
        if (isset($messageClassId)) {
            $params['message_class_id']=$messageClassId;
        }
        if ($this->tableGateway->delete($params)) {
            return true;
        }
        else {
            return null;
        }
    }


}
