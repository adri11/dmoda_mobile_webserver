<?php
namespace Application\Model\DbTable;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Application\Model\MessageClass;

class MessageClassTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter=$this->tableGateway->getAdapter();
    }


    public function fetchAll($eventClassId=null)
    {
        if ($eventClassId) {
            $eventClassId=array('event_class_id'=>$eventClassId);
        }

        $resultSet = $this->tableGateway->select(function (Select $select) use ($eventClassId){
            $select->where($eventClassId)
                ->order('event_class_id ASC');
        });
        return $resultSet;
    }

    public function getElement ($messageClassId) {
        $resultSet = $this->tableGateway->select(array(
            'message_class_id'=>$messageClassId
        ));
        return $resultSet->current();

    }

    public function delElement ($messageClassId) {
        $sql=new Sql($this->adapter);

        $delete=$sql->delete()
            ->from('user_has_message_class')
            ->where(array(
                'message_class_id'=>$messageClassId
            ));
        $statement=$sql->prepareStatementForSqlObject($delete);
        $statement->execute();

        return $this->tableGateway->delete(array('message_class_id'=>$messageClassId));
    }

    public function addMessageClass (MessageClass $data) {
        $data=get_object_vars($data);
        if ($this->tableGateway->insert($data)) {
            //returns the id assigned to the inserted row
            return $this->tableGateway->lastInsertValue;
        }
        else {
            return null;
        }
    }
    public function editMessageClass (MessageClass $data,$messageClassId) {
        $data=get_object_vars($data);
        return $this->tableGateway->update($data,array('message_class_id'=>$messageClassId));
    }
}
