<?php
namespace Application\Model\DbTable;

use Zend\Db\TableGateway\TableGateway;
use Application\Model\EventClass;
use Zend\Db\Sql\Sql;

class EventArchiveTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter=$this->tableGateway->getAdapter();
    }

    public function getListWithEventsDetails()
    {
        $sql = new Sql ($this->adapter);
        $select = $sql->select()
        ->from('event_archive')
        ->join ('event_class','event_class.event_class_id=event_archive.event_class_id')
            ->order(array('event_archive_datetime DESC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }



    public function fetchAllExisting($eventClassId=null)
    {
        $whereArray=array('event_class_deleted'=>false);

        if ($eventClassId) {
            $whereArray['event_class_id']=$eventClassId;
        }

        $resultSet = $this->tableGateway->select($whereArray);
        return $resultSet;
    }

    public function addEventClass (EventClass $data) {
        $data=get_object_vars($data);
        if ($this->tableGateway->insert($data)) {
            //returns the id assigned to the inserted row
            return $this->tableGateway->lastInsertValue;
        }
        else {
            return null;
        }
    }

    public function delElement ($eventClassId) {

        $sql = new Sql ($this->adapter);
        $update = $sql->update('event_class')
            ->set(array(
                'event_class_deleted' => true
            ))
            ->where(array(
                'event_class_id' => $eventClassId
            ));
        $statement = $sql->prepareStatementForSqlObject($update);
        $statement->execute();

        $query="DELETE message_class, user_has_message_class FROM message_class JOIN user_has_message_class ON message_class.message_class_id=user_has_message_class.message_class_id WHERE event_class_id=".$eventClassId;
        $result = $this->adapter->query($query, \Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);


    }

}
