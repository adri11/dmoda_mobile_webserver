<?php
namespace Application\Model\DbTable;

use Zend\Db\TableGateway\TableGateway;
use Application\Model\EventClassHasStore;

class EventClassHasStoreTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();

        return $resultSet;
    }
    public function addEventClassHasStore (EventClassHasStore $data) {
        $data=get_object_vars($data);
        return $this->tableGateway->insert($data);
    }

    public function getListByEventClass($eventClassId)
    {
        $whereArray['event_class_id']=$eventClassId;
        $resultSet = $this->tableGateway->select($whereArray);
        return $resultSet;
    }

}
