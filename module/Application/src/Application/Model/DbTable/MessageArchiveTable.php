<?php
namespace Application\Model\DbTable;

use Zend\Db\TableGateway\TableGateway;
use Application\Model\MessageArchive;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Sql;

class MessageArchiveTable extends AbstractTableGateway
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter=$this->tableGateway->getAdapter();
        $this->table=$this->tableGateway->getTable();
        $this->initialize();
    }
    public function getMessages ($userId=null) {

        $sql=new Sql ($this->adapter);
        $select=$sql->select()
            ->from($this->table)
            ->order ('message_archive_id DESC');

        if (isset($userId)) {
            $select->where(array(
                'user_id'=>$userId
            ));
        }
        else {
            $select->join('user','user.user_id=message_archive.user_id');

        }
        $statement=$sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function addMessage (MessageArchive $data) {
        $data=get_object_vars($data);
        if ($this->tableGateway->insert($data)) {
            //returns the id assigned to the inserted row
            return $this->tableGateway->lastInsertValue;
        }
        else {
            return null;
        }
    }

    public function deleteMessage ($userId=null,$messageArchiveId)
    {
        $params=array();
        if (isset($userId)) {
            $params['user_id']=$userId;
        }
        $params['message_archive_id']=$messageArchiveId;

        if ($this->tableGateway->delete($params)) {
            return true;
        }
        else {
            return null;
        }
    }


}
