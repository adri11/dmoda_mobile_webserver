<?php
namespace Application\Model;
use Zend\Permissions\Acl\Acl as ZendAcl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;

class Acl extends ZendAcl
{
    //TODO da configurare in modo adeguato e attivare all'avvio
	public function __construct()
	{
        $this->addResource(new Resource('Application\Controller\Index'));

		$this->addRole(new Role('public'))
		->allow('public', 'Application\Controller\Index','index');

		$this->addRole(new Role('regular'),'public')
		->allow('regular','Application\Controller\Index',array(
                'home',
                'message-class-list','user-add-message-class-ajax','control-panel','user-edit-regular',
                'message-archive-list','message-archive-delete-ajax',
                'event-class-list','event-class-detail',
                'log-out'
            ));

        //permission on event class form is handled in the view itself
		$this->addRole(new Role('admin'))
		->allow('admin','Application\Controller\Index');

		$this->addRole(new Role('developer'))
		->allow('developer','Application\Controller\Index');
	}

}