<?php
namespace Application\Model;

class MessageSpool
{
    public $message_spool_id;
    public $message_spool_content;
    public $message_spool_programmed_delivery;
    public $user_id;
    public $message_spool_push_sent;


    public function exchangeArray($data) {
        $this->message_spool_id=(isset($data['message_spool_id']))?$data['message_spool_id']:null;
        $this->message_spool_content=(isset($data['message_spool_content']))?$data['message_spool_content']:null;
        $this->message_spool_programmed_delivery=(isset($data['message_spool_programmed_delivery']))?$data['message_spool_programmed_delivery']:null;
        $this->user_id=(isset($data['user_id']))?$data['user_id']:null;
        $this->message_spool_push_sent=(isset($data['message_spool_push_sent']))?$data['message_spool_push_sent']:null;
    }

}
