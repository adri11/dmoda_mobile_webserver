<?php
namespace Application\Model;

class MessageArchive
{
    public $message_archive_id;
    public $message_archive_content;
    public $message_archive_datetime_sent;
    public $user_id;


    public function exchangeArray($data) {
        $this->message_archive_id=(isset($data['message_archive_id']))?$data['message_archive_id']:null;
        $this->message_archive_content=(isset($data['message_archive_content']))?$data['message_archive_content']:null;
        $this->message_archive_datetime_sent=(isset($data['message_archive_datetime_sent']))?$data['message_archive_datetime_sent']:null;
        $this->user_id=(isset($data['user_id']))?$data['user_id']:null;
    }

}
