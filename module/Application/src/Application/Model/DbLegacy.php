<?php
namespace Application\Model;

class DbLegacy
{
    //Same name of DSN defined in ODBC control panel
    private $odbc_name="LIBBALDI";
    private $odbc_connection=NULL;

    /**
     * Open the connection to db
     */
    public function connect () {
        $odbc_connection = odbc_connect($this->odbc_name,'', '');
        if (!($odbc_connection)) {
           $this->odbc_connection=null;
        }
        $this->odbc_connection=$odbc_connection;
    }

    /**
     * Query execution returning array of results
     * @param $query
     * @return array|null
     */
    public function query_exec ($query) {
        if (null===$this->odbc_connection) {
            return null;
        }
        $query_result_set=odbc_exec($this->odbc_connection,$query);
        $query_result_array=array();
        while ($query_result_element=odbc_fetch_array($query_result_set))
        {
            array_push($query_result_array,$query_result_element);
        }
        return $query_result_array;
    }

    public function getDetail ($storeId) {
        $result=$this->query_exec('SELECT NGNEKNEG as legacy_db_store_id,NGNENOM as legacy_db_store_description,NGNELOC as legacy_db_store_city,NGNETIPO as legacy_db_store_type FROM LIBEMME.NGSENE WHERE NGNENOM<>\'\' AND legacy_db_store_id=\''.$storeId.'\'');
        return $result;
    }
    public function getStoresList() {
        $result=$this->query_exec('SELECT NGNEKNEG as legacy_db_store_id,NGNENOM as legacy_db_store_description,NGNELOC as legacy_db_store_city,NGNETIPO as legacy_db_store_type FROM LIBEMME.NGSENE');
        return $result;
    }
    /**
     * Close the connection to db
     */
    public function close(){
        odbc_close($this->odbc_connection);
    }
}
?>