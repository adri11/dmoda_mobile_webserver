<?php
namespace Application\Model;

class MessageClass
{
    public $message_class_id;
    public $message_class_content;
    public $event_class_id;

    public function exchangeArray($data) {
        $this->message_class_id=(isset($data['message_class_id']))?$data['message_class_id']:null;
        $this->message_class_content=(isset($data['message_class_content']))?$data['message_class_content']:null;
        $this->event_class_id=(isset($data['event_class_id']))?$data['event_class_id']:null;

    }

}
