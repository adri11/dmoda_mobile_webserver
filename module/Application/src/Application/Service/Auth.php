<?php

namespace Application\Service;
use Zend\Authentication;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\MvcEvent;
use Application\Model\Acl;
use Zend\View\Helper\FlashMessenger;

class Auth
{
    protected $_utilityModel;
    protected $_auth;
    protected $_authAdapter = null;
    protected $_authService = null;
    private $role;
    private $userId;


    public function __construct(MvcEvent $e=null)
    {
        $this->event=$e;
        $this->sm=$e->getApplication()->getServiceManager();
        $this->userTable = $this->sm->get('Application\Model\DbTable\UserTable');
    }
    
    public function autentica() {
        $controller=$this->event->getRouteMatch()->getParam('controller');
        $action = $this->event->getRouteMatch()->getParam('action');
        
        if (($controller==='Application\Controller\RestMessages')||($controller==='Application\Controller\RestMessagesSubscription')) {
            $userRestToken=$this->event->getRequest()->getQuery('rest-token');
            //TODO filtrare parametro?
            //non ha fatto il log in in precedenza
            if ($userRestToken==null) {
                return null;
            }
            //TODO mi sa che non lo usa, basta fare un return true?
            return $this->userTable->getUserDetailByRestToken($userRestToken);
        }
        else if ($controller==='Application\Controller\RestAccount') {

        }
        else {
            $acl=new Acl();
            if ($this->hasIdentity()) {
                $authService=$this->getAuthService();
                $this->role=$authService->getStorage()->getRole();
                $this->userId=$authService->getStorage()->getUserId();
            }
            else {
                $this->role='public';
                $this->userId=null;
            }
            $viewModel = $this->event->getApplication()->getMvcEvent()->getViewModel();
            $viewModel->role = $this->role;
            $viewModel->userId=$this->userId;

            if (!$acl->hasResource($controller)) {
                throw new \Exception('Resource ' . $controller . ' not defined');
            }

            if (!$acl->isAllowed($this->role, $controller, $action)) {
                $url = $this->event->getRouter()->assemble(array('action'=>'home'), array('name' => 'home'));
                //return $this->redirect()->toRoute('home', array('action' => 'index'));
                $flashMessanger=new \Zend\Mvc\Controller\Plugin\FlashMessenger();
                $flashMessanger->addMessage('Accesso non consentito');
                $response = $this->event->getResponse();
                $response->getHeaders()->addHeaderLine('Location', $url);
                $response->setStatusCode(302);
                $response->sendHeaders();
                exit;
            }
            return true;
        }

    }


    /**
     * Check if Identity is present
     *
     * @return bool
     */
    public function hasIdentity()
    {
        return $this->getAuthService()->hasIdentity();
    }

    /**
     * Return current Identity
     *
     * @return mixed|null
     */
    public function getIdentity()
    {
        return $this->getAuthService()->getIdentity();
    }

    /**
     * Sets Auth Adapter
     *
     * @param \Zend\Authentication\Adapter\DbTable $authAdapter
     * @return UserAuthentication
     */
    public function setAuthAdapter(AuthAdapter $authAdapter)
    {
        $this->_authAdapter = $authAdapter;

        return $this;
    }

    /**
     * Returns Auth Adapter
     *
     * @return \Zend\Authentication\Adapter\DbTable
     */
    public function getAuthAdapter()
    {
        if ($this->_authAdapter === null) {
            $this->setAuthAdapter(new AuthAdapter());
        }

        return $this->_authAdapter;
    }

    /**
     * Sets Auth Service
     *
     * @param \Zend\Authentication\AuthenticationService $authService
     * @return UserAuthentication
     */
    public function setAuthService(AuthenticationService $authService)
    {
        $this->_authService = $authService;

        return $this;
    }

    /**
     * Gets Auth Service
     *
     * @return \Zend\Authentication\AuthenticationService
     */
    public function getAuthService()
    {
        return $this->sm->get('AuthService');
    }
    public function getRole() {
        return $this->role;
    }
}
