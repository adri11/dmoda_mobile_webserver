<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Adri
 * Date: 02/07/13
 * Time: 15.29
 * To change this template use File | Settings | File Templates.
 */

namespace Application\Service;


use Zend\Authentication\Storage\Session;

class AuthStorage extends Session{
    public function setRole ($role) {
        $this->session->offsetSet('role',$role);
    }

    public function getRole() {
        return $this->session->offsetGet('role');
    }

    public function setUserId ($userId) {
        $this->session->offsetSet('userId',$userId);
    }

    public function getUserId() {
        return $this->session->offsetGet('userId');
    }

}