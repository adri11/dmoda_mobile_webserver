<?php
namespace Application\Form;
use Zend\InputFilter\InputFilter;

class EventClassFilter extends InputFilter
{
    public function __construct()
    {
        //necessario perchè è un fieldset
        $common=new InputFilter();

        $common->add(array(
            'name' => 'event_class_name',
            'required' => true,
            'filters' => array(
                array('name' => 'StringTrim'),
                array('name' => 'StripTags'),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 5,
                        'max' => 40,
                    ),
                ),

            ),
        ));

        $common->add(array(
            'name' => 'event_class_description',
            'required' => false,
            'filters' => array(
                array('name' => 'StringTrim'),
                array('name' => 'StripTags'),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max' => 100,
                    ),
                ),
            ),
        ));

        $common->add(array(
            'name' => 'event_class_check_interval',
            'required' => true,
        ));

        $common->add(array(
            'name' => 'event_class_period_type',
            'required' => true,
        ));
        $common->add(array(
            'name' => 'event_class_period_days',
            'required' => false,
        ));
        $common->add(array(
            'name' => 'event_class_period_date_start',
            'required' => false,
            'allow_empty'=> false,
        ));
        $common->add(array(
            'name' => 'event_class_period_date_end',
            'required' => false,
            'allow_empty'=> false,
        ));


        $common->add(array(
            'name' => 'event_class_type',
            'required' => true,
        ));

        //necessario perchè è un fieldset
        $this->add($common,'common');
//***********    STRUCTURED ************************************

        $structured=new InputFilter();

        $structured->add(array(
            'name' => 'event_class_int_reference_value',
            'required' => true,
            'allow_empty'=> false,
            'validators' => array(
                array(
                    'name' => 'Digits',
                ),

            ),
        ));


        $structured->add(array(
            'name' => 'event_class_structured_comparision_type',
            'required' => true,
        ));

        $structured->add(array(
            'name' => 'event_class_structured_value_type',
            'required' => true,
        ));

        $structured->add(array(
            'name' => 'legacy_db_store_id',
            'required' => false,
            'allow_empty'=>false,
        ));

        $structured->add(array(
            'name' => 'event_class_structured_all_stores',
            'required' => false,
            'allow_empty'=>false,
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' =>array (
                        'messages'=>array(
                            \Zend\Validator\NotEmpty::IS_EMPTY =>'Selezionare almeno un negozio o tutti',
                        ),
                    )
                ),
            )
        ));


        //necessario perchè è un fieldset
        $this->add($structured,'structured');
//***********    COMPARED QUERY ************************************
        $comparedQuery=new InputFilter();



        $comparedQuery->add(array(
            'name' => 'event_class_query',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 5,
                        'max' => 300,
                    ),
                ),
            ),
        ));

        $comparedQuery->add(array(
            'name' => 'event_class_compared_query_comparision_type',
            'required' => true,
        ));

        $comparedQuery->add(array(
            'name' => 'event_class_compared_query_string_reference_value',
            'required' => false,
            'allow_empty'=> false,
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max' => 100,
                    ),
                ),
            ),

        ));

        $comparedQuery->add(array(
            'name' => 'event_class_int_reference_value',
            'required' => false,
            'allow_empty'=> false,
            'validators' => array(
                array(
                    'name' => 'Digits',
                ),

            ),
        ));


        //necessario perchè è un fieldset
        $this->add($comparedQuery,'compared_query');

//***********    RAW QUERY ************************************
        $rawQuery=new InputFilter();

        $rawQuery->add(array(
            'name' => 'event_class_query',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max' => 300,
                    ),
                ),
            ),
        ));

        //necessario perchè è un fieldset
        $this->add($rawQuery,'raw_query');


    }
}

?>
