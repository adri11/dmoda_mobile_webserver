<?php
namespace Application\Form;
use Zend\InputFilter\InputFilter;

class MessageClassFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(array(
            'name' => 'message_class_content',
            'required' => true,
            'filters' => array(
                array('name' => 'StringTrim'),
                array('name' => 'StripTags'),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'max' => 1000,
                    ),
                ),
            ),
        ));

    }
}

?>
