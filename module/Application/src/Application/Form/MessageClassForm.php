<?php
namespace Application\Form;
use Zend\Form\Form;

class MessageClassForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('message_class');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'message_class_id',
            'type' => 'hidden',
            'attributes' => array(),
        ));
        $this->add(array(
            'name' => 'event_class_id',
            'type' => 'hidden',
            'attributes' => array(),
        ));
        $this->add(array(
            'name' => 'message_class_content',
            'type' => 'textarea',
            'attributes' => array(
                'style'=>'width: 600px'
            ),
            'options' => array(
                'label' => 'Testo del messaggio',
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Invia',
                'class'=>'submit'
            ),
            'options' => array()
        ));

    }
}