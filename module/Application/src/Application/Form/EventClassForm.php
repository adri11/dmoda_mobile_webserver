<?php
namespace Application\Form;
use Zend\Form\Form;
use Zend\Form\Fieldset;

class EventClassForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('user');
        $this->setAttribute('method', 'post');
        $commonFieldSet=new Fieldset('common');
        $commonFieldSet->add(array(
            'name' => 'event_class_id',
            'type' => 'hidden',
            'attributes' => array(),
        ));
        $commonFieldSet->add(array(
            'name' => 'event_class_name',
            'type' => 'text',
            'attributes' => array(
                'style'=>'width: 600px'
            ),
            'options' => array(
                'label' => 'Nome evento (es. Roma vendite min. 10000)',
            )
        ));
        $commonFieldSet->add(array(
            'name' => 'event_class_description',
            'type' => 'text',
            'attributes' => array(
                'style'=>'width: 600px'
            ),
            'options' => array(
                'label' => 'Descrizione evento (es. Verifica giornalmente che il negozio etc.)',
            )
        ));

        $commonFieldSet->add(array(
            'name' => 'event_class_check_interval',
            'type' => 'Select',
            'attributes' => array(),
            'options' => array(
                'label' => 'Intervallo di verifica (in minuti)',
                'value_options' => array(
                    1 =>'1',
                    2 => '2',
                    3 => '3',
                    4=>'4',
                    5=>'5',
                    10=>'10',
                    20=>'20',
                    30=>'30',
                    60=>'60',
                    120=>'120',
                    /*999=>'Fine giornata' //Per ora meglio evitare perchè incasina abbastanza le query*/
                )
            )
        ));

        $commonFieldSet->add(array(
            'name' => 'event_class_period_type',
            'type' => 'Radio',
            'attributes' => array(),
            'options' => array(
                'label' => 'Tipologia di periodo',
                'value_options' => array(
                    'days'=>'Ogni tot giorni',
                    'dates'=>'Intervallo di date',
                )
            )
        ));

        $commonFieldSet->add(array(
            'name' => 'event_class_period_days',
            'type' => 'Select',
            'attributes' => array(),
            'options' => array(
                'label' => 'Numero giorni',
                'value_options' => array(
                    1=>'1',
                    2=>'2',
                    3=>'3',
                    4=>'4',
                    5=>'5',
                    6=>'6',
                    7=>'7',
                    8=>'8',
                    9=>'9',
                    10=>'10'
                )
            )
        ));

        $commonFieldSet->add(array(
            'name' => 'event_class_period_date_start',
            'type' => 'text',
            'attributes' => array(),
            'options' => array(
                'label' => 'Data di inizio',
            )
        ));

        $commonFieldSet->add(array(
            'name' => 'event_class_period_date_end',
            'type' => 'text',
            'attributes' => array(),
            'options' => array(
                'label' => 'Data di fine',
            )
        ));

        $commonFieldSet->add(array(
            'name' => 'event_class_type',
            'type' => 'Radio',
            'attributes' => array(
            ),
            'options' => array(

                'label' => 'Tipologia di evento',
                'value_options' => array(
                    'structured'=>'Strutturato',
                    'compared_query'=>'Query comparata',
                    'raw_query'=>'Query grezza'
                )
            )
        ));
        $this->add($commonFieldSet);

// ****************** STRUCTURED FIELD SET *******************************

        $structuredFieldSet=new Fieldset('structured');
        $structuredFieldSet->add(array(
            'name' => 'event_class_structured_comparision_type',
            'type' => 'Select',
            'attributes' => array(),
            'options' => array(
                'label' => 'Tipo di confronto',
                'value_options' => array(
                    'min_reached'=>'Minimo raggiunto',
                    'min_not_reached'=>'Minimo non raggiunto',
                    'delta'=>'A delta',
                )
            )
        ));
        $structuredFieldSet->add(array(
            'name' => 'event_class_int_reference_value',
            'type' => 'text',
            'attributes' => array(),
            'options' => array(
                'label' => 'Valore di riferimento',
            )
        ));
        $structuredFieldSet->add(array(
            'name' => 'event_class_structured_value_type',
            'type' => 'Select',
            'attributes' => array(),
            'options' => array(
                'label' => 'Tipo di grandezza',
                'value_options' => array(
                    'money'=>'Incasso (in €)',
                    'product_quantity'=>'Prodotti venduti (es. 50 paia)',
                    'transaction_quantity'=>'Transazioni effettuate (es. 50 scontrini battuti)',
                )
            )
        ));


        $structuredFieldSet->add(array(
            'name' => 'legacy_db_store_id',
            'type' => 'Multicheckbox',
            'options' => array(
                'label' => 'Seleziona i negozi',
                'value_options' => array(
                    1=>'negozio1',
                    2=>'negozio2',
                    3=>'negozio3',
                    4=>'negozio4',

                    //Values added at runtime by extracting from legacy db
                )
            )
        ));
        $structuredFieldSet->add(array(
            'name' => 'event_class_structured_all_stores',
            'type' => 'Checkbox',
            'attributes' => array(
                'class'=>'prova'
            ),
            'options' => array(
                'label' => 'Tutti i negozi',
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value'=>false,
            )
        ));

        $this->add($structuredFieldSet);

//********************** COMPARED QUERY FIELD SET *******************************
        $comparedQueryFieldSet=new Fieldset('compared_query');
        $comparedQueryFieldSet->add(array(
            'name' => 'event_class_compared_query_comparision_type',
            'type' => 'Select',
            'attributes' => array(),
            'options' => array(
                'label' => 'Tipo di confronto',
                'value_options' => array(
                    //se cambi l'ordine questo comporta una variazione nel codice js della view
                    //va verificato quale campo nascondere e quale visualizzare
                    'min_reached'=>'Minimo raggiunto',
                    'min_not_reached'=>'Minimo non raggiunto',
                    'delta'=>'A delta',
                    'string_contained'=>'Stringa contenuta nei risultati'
                )
            )
        ));
        $comparedQueryFieldSet->add(array(
            'name' => 'event_class_int_reference_value',
            'type' => 'text',
            'attributes' => array(),
            'options' => array(
                'label' => 'Valore di riferimento',
            )
        ));

        $comparedQueryFieldSet->add(array(
            'name' => 'event_class_compared_query_string_reference_value',
            'type' => 'text',
            'attributes' => array(
                'style'=>'width: 600px'
            ),
            'options' => array(
                'label' => 'Stringa ricercata',
            )
        ));

        $comparedQueryFieldSet->add(array(
            'name' => 'event_class_query',
            'type' => 'text',
            'attributes' => array(
                'style'=>'width: 600px'
            ),
            'options' => array(
                'label' => 'Query da verificare (il risultato dev\' essere messo come alias "result" (es. SELECT margine_netto AS result ...))',
            )
        ));




        $this->add($comparedQueryFieldSet);


//**********************  RAW QUERY FIELD SET  **********************************
        $rawQueryFieldSet=new Fieldset('raw_query');
        $rawQueryFieldSet->add(array(
            'name' => 'event_class_query',
            'type' => 'text',
            'attributes' => array(
                'style'=>'width: 600px'
            ),
            'options' => array(
                'label' => 'Query da verificare (il risultato dev\' essere messo come alias "result" (es. SELECT margine_netto AS result ...))',
            )
        ));

        $this->add($rawQueryFieldSet);
//************************ FINE  ************************************************

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Invia',
                'class'=>'submit'
            ),
            'options' => array()
        ));

    }
}