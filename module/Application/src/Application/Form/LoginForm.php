<?php
namespace Application\Form;
use Zend\Form\Form;

class LoginForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('user');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'user_username',
            'type' => 'text',
            'attributes' => array(),
            'options' => array(
                'label' => 'Nome utente',
            )
        ));
        $this->add(array(
            'name' => 'user_password',
            'type' => 'password',
            'attributes' => array(),
            'options' => array(
                'label' => 'Password',
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Invia',
                'class'=>'submit'
            ),
            'options' => array()
        ));

    }
}