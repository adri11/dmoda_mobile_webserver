<?php
namespace Application\Form;
use Zend\Form\Form;

class UserFormRegular extends UserFormSuper
{
    public function __construct($name = null)
    {
        parent::__construct('user');

        $this->remove('user_type');

    }
}