<?php
namespace Application\Form;
use Zend\Form\Form;
use Zend\Form\Fieldset;

class UserMessageClassForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'message_class_id',
            'type' => 'hidden',
            'attributes' => array(),
        ));

        $this->add(array(
            'name' => 'user_has_message_class_interval',
            'type' => 'Select',
            'attributes' => array(),
            'options' => array(
                //'label' => 'Tempo di notifica (in minuti)',
                'value_options' => array(
                    999 =>'Disabilitata',
                    0=>'Immediata',
                    998=>'A fine giornata',
                    5=>'5 minuti',
                    10=>'10 minuti',
                    20=>'20 minuti',
                    30=>'30 minuti',
                    60=>'1 ora',
                    120=>'2 ore',
                    180=>'3 ore'
                )
            )
        ));


    }
}