<?php
namespace Application\Form;
use Zend\Form\Form;

class UserFormSuper extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('user');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'user_id',
            'type' => 'hidden',
            'attributes' => array(),
        ));
        $this->add(array(
            'name' => 'user_description',
            'type' => 'text',
            'attributes' => array(
                'style'=>'width: 600px'
            ),
            'options' => array(
                'label' => 'Descrizione (ad es. Nome e Cognome)',
            )
        ));
        $this->add(array(
            'name' => 'user_type',
            'type' => 'Select',
            'attributes' => array(),
            'options' => array(
                'label' => 'Tipo di utenza',
                'value_options' => array(
                    'regular'=>'Regolare',
                    'admin'=>'Amministratore',
                    'developer'=>'Sviluppatore',
                )
            )
        ));
        $this->add(array(
            'name' => 'user_smartphone_type',
            'type' => 'Select',
            'attributes' => array(),
            'options' => array(
                'label' => 'Sistema operativo smartphone',
                'value_options' => array(
                    'ios'=>'iOS',
                    'android'=>'Android'
                )
            )
        ));
        $this->add(array(
            'name' => 'user_username',
            'type' => 'text',
            'attributes' => array(),
            'options' => array(
                'label' => 'Nome utente',
            )
        ));
        $this->add(array(
            'name' => 'user_password',
            'type' => 'password',
            'attributes' => array(),
            'options' => array(
                'label' => 'Password',
            )
        ));
        $this->add(array(
            'name' => 'user_password_check',

            'type' => 'password',
            'attributes' => array(
                'id'=>'user_password_check',
            ),
            'options' => array(
                'label' => 'Conferma password',
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Invia',
                'class'=>'submit'
            ),
            'options' => array()
        ));

    }
}