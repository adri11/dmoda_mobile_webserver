<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/[:action]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'events' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/events[/:action]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'=>'event-class-list'

                    ),
                ),
                'may_terminate'=>true,
                'child_routes' => array(
                    'wildcard' => array(
                        'type' => 'Zend\Mvc\Router\Http\Wildcard',
                    ),
                ),
            ),
            'users' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/users[/:action]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'=>'user-list'

                    ),
                ),
                'may_terminate'=>true,
                'child_routes' => array(
                    'wildcard' => array(
                        'type' => 'Zend\Mvc\Router\Http\Wildcard',
                    ),
                ),
            ),
            'messages' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/messages[/:action]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'=>'message-list'

                    ),
                ),
                'may_terminate'=>true,
               'child_routes' => array(
                    'wildcard' => array(
                        'type' => 'Zend\Mvc\Router\Http\Wildcard',
                    ),
                ),
            ),
            'rest' => array(
                'type'    => 'Literal',
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/rest',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'RestMessages',
                    ),
                ),

                'may_terminate' => true,
                'child_routes' => array(
                    'messages' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/messages[/:id]',
                            'defaults' => array(
                                'controller' => 'RestMessages',
                            ),
                        ),
                    ),
                    'messages-subscription' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/messages-subscription[/:id]',
                            'defaults' => array(
                                'controller' => 'RestMessagesSubscription',
                            ),
                        ),
                    ),
                    'account' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/account[/:id]',
                            'defaults' => array(
                                'controller' => 'RestAccount',
                            ),
                        ),
                    ),
                    'event-class' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/event-class[/:id]',
                            'defaults' => array(
                                'controller' => 'RestEventClass',
                            ),
                        ),
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),

    ),
    'translator' => array(
        'locale' => 'it',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),

    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\RestMessages' => 'Application\Controller\RestMessagesController',
            'Application\Controller\RestMessagesSubscription' => 'Application\Controller\RestMessagesSubscriptionController',
            'Application\Controller\RestAccount' => 'Application\Controller\RestAccountController',
            'Application\Controller\RestEventClass' => 'Application\Controller\RestEventClassController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'view_helpers' => array(
        'invokables'=>array(
            'formatDateTime'=>'Application\View\Helper\FormatDateTime',
            'getRole'=>'Application\View\Helper\GetRole'
        )
    ),
);
