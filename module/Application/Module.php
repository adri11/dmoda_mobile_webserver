<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
//TODO Riattivare blocco rest su porta 80 su apache
namespace Application;

use Application\Service\Auth;
use Application\Service\AuthStorage;
use Application\View\Helper\GetRole;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Validator\Authentication;
use Zend\ModuleManager\ModuleEvent;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager;

use Application\Model\User;
use Application\Model\DbTable\UserTable;
use Application\Model\EventClass;
use Application\Model\DbTable\EventClassTable;
use Application\Model\MessageClass;
use Application\Model\DbTable\MessageClassTable;
use Application\Model\UserHasMessageClass;
use Application\Model\DbTable\UserHasMessageClassTable;
use Application\Model\EventClassHasStore;
use Application\Model\DbTable\EventClassHasStoreTable;
use Application\Model\MessageSpool;
use Application\Model\DbTable\MessageSpoolTable;
use Application\Model\MessageArchive;
use Application\Model\DbTable\MessageArchiveTable;
use Application\Model\EventArchive;
use Application\Model\DbTable\EventArchiveTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\I18n\Translator\Translator;
use Zend\Authentication\Adapter\DbTable as DbAuthAdapter;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {

        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $prova = new Translator();
        //Va a cercare il locale it_IT, non it, di conseguenza la cartella deve chiamarsi it_IT
        $prova->addTranslationFilePattern(
            'phpArray',
            'vendor/zendframework/zendframework/resources/languages/',
            '%s/Zend_Validate.php');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($prova);
        ini_set('date.timezone','Europe/Rome');

        $eventManager->attach(MvcEvent::EVENT_DISPATCH, function (MvcEvent $e) {
            $authService = new Auth($e);
            if (!$authService->autentica()) {
                exit;
            }
        }, 1);
        $serviceManager = $e->getApplication()->getServiceManager();
        $serviceManager->get('viewhelpermanager')->setFactory('GetRole', function ($sm) use ($e) {
            return new GetRole($sm);
        });
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Application\Model\DbTable\UserTable' => function ($sm) {
                    $tableGateway = $sm->get('UserTableGateway');
                    $table = new UserTable($tableGateway);
                    return $table;
                },
                'UserTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new User());
                    return new TableGateway('user', $dbAdapter, null, $resultSetPrototype);
                },
                'Application\Model\DbTable\EventClassTable' => function ($sm) {
                    $tableGateway = $sm->get('EventClassTableGateway');
                    $table = new EventClassTable($tableGateway);
                    return $table;
                },
                'EventClassTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new EventClass());
                    return new TableGateway('event_class', $dbAdapter, null, $resultSetPrototype);
                },
                'Application\Model\DbTable\MessageClassTable' => function ($sm) {
                    $tableGateway = $sm->get('MessageClassTableGateway');
                    $table = new MessageClassTable($tableGateway);
                    return $table;
                },
                'MessageClassTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MessageClass());
                    return new TableGateway('message_class', $dbAdapter, null, $resultSetPrototype);
                },
                'Application\Model\DbTable\UserHasMessageClassTable' => function ($sm) {
                    $tableGateway = $sm->get('UserHasMessageClassTableGateway');
                    $table = new UserHasMessageClassTable($tableGateway);
                    return $table;
                },
                'UserHasMessageClassTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new UserHasMessageClass());
                    return new TableGateway('user_has_message_class', $dbAdapter, null, $resultSetPrototype);
                },
                'Application\Model\DbTable\EventClassHasStoreTable' => function ($sm) {
                    $tableGateway = $sm->get('EventClassHasStoreTableGateway');
                    $table = new EventClassHasStoreTable($tableGateway);
                    return $table;
                },
                'EventClassHasStoreTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new EventClassHasStore());
                    return new TableGateway('event_class_has_store', $dbAdapter, null, $resultSetPrototype);
                },
                'Application\Model\DbTable\MessageSpoolTable' => function ($sm) {
                    $tableGateway = $sm->get('MessageSpoolTableGateway');
                    $table = new MessageSpoolTable($tableGateway);
                    return $table;
                },
                'MessageSpoolTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MessageSpool());
                    return new TableGateway('message_spool', $dbAdapter, null, $resultSetPrototype);
                },
                'Application\Model\DbTable\MessageArchiveTable' => function ($sm) {
                    $tableGateway = $sm->get('MessageArchiveTableGateway');
                    $table = new MessageArchiveTable($tableGateway);
                    return $table;
                },
                'MessageArchiveTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MessageArchive());
                    return new TableGateway('message_archive', $dbAdapter, null, $resultSetPrototype);
                },
                'Application\Model\DbTable\EventArchiveTable' => function ($sm) {
                    $tableGateway = $sm->get('EventArchiveTableGateway');
                    $table = new EventArchiveTable($tableGateway);
                    return $table;
                },
                'EventArchiveTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new EventArchive());
                    return new TableGateway('event_archive', $dbAdapter, null, $resultSetPrototype);
                },
                'Auth' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $dbAuthAdapter = new DbAuthAdapter($dbAdapter,'user','user_username','user_password','MD5(?)');
                    return $dbAuthAdapter;
                },
                'AuthService' => function ($sm) {
                    $authService=new AuthenticationService(new AuthStorage());
                    return $authService;
                }

            ));
    }
}
